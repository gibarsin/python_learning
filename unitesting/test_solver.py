from unittest import TestCase
from unitesting.Solver import Solver


class TestSolver(TestCase):
    def test_negative_roots(self):
        solver = Solver

        # throws and "catches" exception with assert
        self.assertRaises(Exception, solver.pol_roots_2, 2, 1, 2)

    def test_pol_roots_2(self):
        self.fail()  # should always fail
