import math


class Solver:
    def pol_roots_2(self, a, b, c):
        d = b ** 2 - 4 * a * c

        if d >= 0:
            disc = math.sqrt(d)
            root1 = (-b + disc) / (2 * a)
            root2 = (-b - disc) / (2 * a)
        else: # complex roots
            raise Exception

Solver().pol_roots_2(2, 1, 0)
