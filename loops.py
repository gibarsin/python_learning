cars = ['Honda', 'Mazda', 'Toyota']

# Testing for each loop

year_start = 2010

for car in cars:
    car_year = car + str(year_start)
    year_start += 1

    print(car_year)

# Should be false
if 'ferrari' in cars:
    print("Expensive")
else:
    print("Cheap")

cars.append('ferrari')

if 'ferrari' in cars:
    print("Expensive") # Should print
else:
    print("Cheap")

cars.remove('Honda')

# Should be false
print('Honda') in cars