# Testing dynamic types

a = 10

print(a)

a = "Hello World"

print(a)

# Testing way of calling
print(len(a))
print(len(a))

b = 5

c = " Bye"

# Testing (errors in) overloadable operators
# b + a
# a + b

print(a + c)

d = 5

# Testing repeat operator (neat)
print(a * d)

#erase var definition
del d

# print d #Should give an error because it is not defined