# Testing function that doesn't exist
# foo

# Testing function call
import sys


def foo():
    print("foo")


foo


# Testing recursive call
def rec_foo(x):
    if x == 0:
        return
    print(x)
    rec_foo(x - 1)


# Testing boilerplate
def main():
    print(sys.argv)
    rec_foo(5)


if __name__ == '__main__':
    main()
