# Works like a map

dict_movies = {} # Create empty dictionary

dict_movies['LOTR'] = 'Epic'
dict_movies['Matrix'] = 'Science Fiction?'
dict_movies['Creed'] = 'Fight'

print(dict_movies) # Prints every key-value pair

# Try to add a list in a value
dict_movies['Harry Potter'] = ['Adventure', 'Fantasy'] # Adds it okay!

print(dict_movies)

print(dict_movies.get('Harry Potter'))

movie_1 = 'Revenant'

# print dict_movies[movie_1] # KeyError (doesn't exist)

# Avoid KeyError
if movie_1 in dict_movies:
    print(dict_movies[movie_1])
else:
    print(movie_1 + ' is not listed')

print(dict_movies.keys())

print(dict_movies.values())

# Iterate over keys

for key_movie in dict_movies:
    print(key_movie)

for key_movie in dict_movies.keys():
    print(key_movie)

# This won't work because there is a list in the values
#for key_movie, value_movie in dict_movies.items(): # items are entries (key-pair value)
#    print 'Key: ' + key_movie + ', Value: ' + value_movie

print(dict_movies.items())  # Different way of printing entries

# Formatting (doesn't look very useful)
info = '%(LOTR)s' %dict_movies
print(info)

# Deletion

movie_2 = 'LOTR'

del dict_movies['LOTR']

if movie_2 in dict_movies:
    print(movie_2 + ' is still here')
else:
    print(movie_2 + ' has been successfully deleted')